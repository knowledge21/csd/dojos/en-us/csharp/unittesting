namespace SalesSystem
{
    public class CommissionCalculator
    {
        public double calculate(int saleValue)
        {
            if (saleValue > 10000) return 0.06 * saleValue;
            return 25;
        }

    }
    
}