using Microsoft.VisualStudio.TestTools.UnitTesting;
using SalesSystem;

namespace SalesSystem.Tests
{
    [TestClass]
    public class CommissionCalculatorTests
    {
        [TestMethod]
        public void Test500USDSalePays25USDCommission()
        {
            int saleValue = 500;
            int expectedCommission = 25;

            var actualCommission = new CommissionCalculator().calculate(saleValue);

            Assert.AreEqual(expectedCommission, actualCommission);
        }

        [TestMethod]
        public void Test12000USDSalePays720USDCommission()
        {
            int saleValue = 12000;
            int expectedCommission = 720;

            var actualCommission = new CommissionCalculator().calculate(saleValue);

            Assert.AreEqual(expectedCommission, actualCommission);
        }

        [TestMethod]
        public void Test10001USDSalePays600_06USDCommission()
        {
            int saleValue = 10001;
            double expectedCommission = 600.06;

            var actualCommission = new CommissionCalculator().calculate(saleValue);

            Assert.AreEqual(expectedCommission, actualCommission);
        }

    }
}
